
### Steps to Set Up Dotfiles with a Bare Git Repository

#### 1. Create a Bare Git Repository
A bare Git repository tracks files in your home directory without creating a `.git` directory inside it.

```zsh
git init --bare $HOME/.dotfiles
```

This initializes a bare Git repository in `~/.dotfiles`.

#### 2. Set Up an Alias for Convenience
To make working with the bare repository easier, create an alias for Git commands that use the `.dotfiles` repository:

```zsh
alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
```

Persist the alias by adding it to your shell configuration file (e.g., `~/.bashrc` or `~/.zshrc`):

```zsh
echo "alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> ~/.bashrc
source ~/.zshrc
```

#### 3. Ignore the `.dotfiles` Directory
Prevent recursive issues by ignoring the `.dotfiles` directory:

```zsh
echo ".dotfiles" >> $HOME/.gitignore
dotfiles add .gitignore
dotfiles commit -m "Exclude the .dotfiles folder"
```

#### 4. Add and Commit Your Dotfiles
Track your configuration files (dotfiles) by adding them to the repository:

```zsh
dotfiles add .zshrc
dotfiles add .vimrc
dotfiles commit -m "Add initial dotfiles"
```

Repeat this step for all files or directories you want to track.

#### 5. Add a Remote Repository on GitLab
Set up a repository on GitLab to store your dotfiles. Add the GitLab repository as a remote:

```zsh
dotfiles remote add origin git@gitlab.com:your-username/dotfiles.git
```

Replace `your-username` with your GitLab username.

#### 6. Push the Dotfiles to GitLab
Rename the default branch if necessary (e.g., from `master` to `main`), then push the branch:

```zsh
dotfiles branch -M main  # Rename branch to 'main'
dotfiles push -u origin main
```

If the branch is already `master`, you can push it directly:

```zsh
dotfiles push -u origin master
```

#### 7. Set Up SSH Authentication (If Needed)
If you encounter a `Permission denied (publickey)` error, follow these steps:
- Generate an SSH key:
  ```zsh
  ssh-keygen -t rsa -b 4096 -C "your.email@example.com"
  ```
- Add the key to the SSH agent:
  ```zsh
  eval "$(ssh-agent -s)"
  ssh-add ~/.ssh/id_rsa
  ```
- Add the public key (`~/.ssh/id_rsa.pub`) to your GitLab account under **Settings > SSH Keys**.

Verify the SSH connection:

```zsh
ssh -T git@gitlab.com
```

#### 8. Add More Files or Directories
To add new files or directories, use:

```zsh
dotfiles add <file_or_directory>
dotfiles commit -m "Add description of changes"
dotfiles push
```

For example:

```zsh
dotfiles add .config/nvim
dotfiles commit -m "Added nvim configuration directory"
dotfiles push
```

---

### Using Dotfiles on a New Machine

1. Clone the repository as a bare repository:

   ```zsh
   git clone --bare git@gitlab.com:your-username/dotfiles.git $HOME/.dotfiles
   ```

2. Set up the `dotfiles` alias:

   ```zsh
   alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
   ```

3. Checkout the repository contents:

   ```zsh
   dotfiles checkout
   ```

   If there are conflicts, back up or remove existing files before retrying.

4. Hide untracked files in the repository:

   ```zsh
   dotfiles config --local status.showUntrackedFiles no
   ```

---

### Additional Tips
- **Updating Files**: Use `dotfiles add` and `dotfiles commit` whenever you modify or add new files.
- **Removing Files**: To stop tracking a file, use:
  ```zsh
  dotfiles rm --cached <file>
  ```
- **Backup and Restore**: Your repository on GitLab serves as a backup. To restore, repeat the **"Using Dotfiles on a New Machine"** steps.